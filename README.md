### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying NewPipe, a libre lightweight streaming frontend for Android.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/TeamNewPipe/NewPipe).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/NewPipe/-/commit/ac3563de03303db488de73c290078f801464db23)):

1. Added maven repository and Codavel's Application ID and Secret to the app's build.gradle, required to download and use Codavel's SDK.
2. Start Codavel's Service when the main activity is created, so that our SDK can start processing the HTTP requests.
3. Register our HTTP interceptor into the app's default OkHTTP Builder, so that all the HTTP requests executed through the app's OKHTTPClient are processed and forwarded to our SDK.
4. Modify Exoplayer's data source so that it uses an OkHTTP Client also registered with our HTTP interceptor.
